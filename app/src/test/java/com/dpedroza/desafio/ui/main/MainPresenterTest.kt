package com.dpedroza.desafio.ui.main

import com.dpedroza.desafio.data.repository.GitRepository
import com.dpedroza.desafio.ui.base.BaseMvpPresenter
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.ArgumentMatchers.anyObject
import org.mockito.Mock
import org.mockito.Mockito.never
import org.mockito.Mockito.verify
import org.mockito.junit.MockitoJUnitRunner
import javax.inject.Inject


@RunWith(MockitoJUnitRunner::class)
class MainPresenterTest {

  @Inject
  @Mock
  lateinit var gitRepository: GitRepository

  @Mock
  lateinit var view: MainContract.View

  lateinit var presenter: MainPresenter

  @Before
  @Throws(Exception::class)
  fun setUp() {

    presenter = MainPresenter(gitRepository)
    presenter.attachView(view)

  }

  @Test
  @Throws(Exception::class)
  fun viewAttachedToPresenter() {

    assertEquals(presenter.view, view)

  }

  @Test(expected=BaseMvpPresenter.MvpViewNotAttachedException::class)
  fun viewDettachedFromPresenterMethodCallThrownError() {

    presenter.detachView()
    presenter.getTopRepositories(1, true)

    verify(view, never()).onLoadStarted()

  }

}