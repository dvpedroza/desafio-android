package com.dpedroza.desafio.di.components

import android.content.Context
import com.dpedroza.desafio.application.DesafioApplication
import com.dpedroza.desafio.di.modules.ApplicationModule
import com.dpedroza.desafio.di.modules.NetworkModule
import com.dpedroza.desafio.di.modules.RepositoryModule
import com.dpedroza.desafio.di.scopes.ApplicationContext
import dagger.Component
import javax.inject.Singleton

/**
 * A component whose lifetime is the life of the application.
 */
@Singleton // Constraints this component to one-per-application or unscoped bindings.
@Component(modules = arrayOf(ApplicationModule::class, NetworkModule::class, RepositoryModule::class))
interface ApplicationComponent {

    fun desafioApplication(): DesafioApplication

    fun activityComponent(): ActivityComponent

    @ApplicationContext fun context(): Context

}
