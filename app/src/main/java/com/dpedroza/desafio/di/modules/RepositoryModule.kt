package com.dpedroza.desafio.di.modules

import com.dpedroza.desafio.data.repository.GitRepository
import com.dpedroza.desafio.data.repository.PullRequestRepository
import com.dpedroza.desafio.data.repository.manager.GitManager
import com.dpedroza.desafio.data.repository.manager.PullRequestManager
import com.dpedroza.desafio.data.service.GitService
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class RepositoryModule {

  @Provides @Singleton fun provideGitRepository(service: GitService) : GitRepository {
    return GitManager(service)
  }

  @Provides @Singleton fun providePullRequestRepository(service: GitService) : PullRequestRepository {
    return PullRequestManager(service)
  }

}