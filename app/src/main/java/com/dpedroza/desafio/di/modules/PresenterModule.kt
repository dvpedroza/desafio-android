package com.dpedroza.desafio.di.modules

import com.dpedroza.desafio.data.repository.GitRepository
import com.dpedroza.desafio.data.repository.PullRequestRepository
import com.dpedroza.desafio.di.scopes.PerActivity
import com.dpedroza.desafio.ui.detail.DetailPresenter
import com.dpedroza.desafio.ui.main.MainPresenter
import dagger.Module
import dagger.Provides

@Module
class PresenterModule {

  @Provides @PerActivity fun providesMainPresenter(gitRepository: GitRepository) : MainPresenter {
    return MainPresenter(gitRepository)
  }

  @Provides @PerActivity fun providesDetailPresenter(gitRepository: PullRequestRepository) : DetailPresenter {
    return DetailPresenter(gitRepository)
  }
}