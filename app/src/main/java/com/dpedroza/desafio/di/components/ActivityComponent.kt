package com.dpedroza.desafio.di.components

import com.dpedroza.desafio.di.modules.PresenterModule
import com.dpedroza.desafio.di.scopes.PerActivity
import com.dpedroza.desafio.ui.detail.DetailActivity
import com.dpedroza.desafio.ui.main.MainActivity
import dagger.Subcomponent


/**
 * This component inject dependencies to all Activities across the application
 */
@PerActivity @Subcomponent(modules = arrayOf(PresenterModule::class))
interface ActivityComponent {

  fun inject(mainActivity: MainActivity)
  fun inject(detailActivity: DetailActivity)

}