package com.dpedroza.desafio.di.modules

import com.dpedroza.desafio.application.DesafioApplication
import com.dpedroza.desafio.data.service.GitService
import com.google.gson.Gson
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Singleton


@Module class NetworkModule {

  @Provides @Singleton fun provideOkHttpCache(application: DesafioApplication): Cache {
    val cacheSize = 10 * 1024 * 1024
    return okhttp3.Cache(application.cacheDir, cacheSize.toLong())
  }

  @Provides @Singleton fun provideGson(): com.google.gson.Gson {
    val gsonBuilder = com.google.gson.GsonBuilder()
    return gsonBuilder.create()
  }

  @Provides @Singleton fun provideOkHttpClient(cache:Cache): OkHttpClient {

    val httpLoggingInterceptor = okhttp3.logging.HttpLoggingInterceptor()
    httpLoggingInterceptor.level = okhttp3.logging.HttpLoggingInterceptor.Level.BODY
    val builder = okhttp3.OkHttpClient.Builder()
    builder.addInterceptor(httpLoggingInterceptor)
    builder.cache(cache)

    return builder.build()
  }

  @Provides @Singleton fun provideRetrofit(gson: Gson,
      okHttpClient: OkHttpClient): Retrofit {
    return retrofit2.Retrofit.Builder().baseUrl("https://api.github.com/")
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(gson))
        .client(okHttpClient)
        .build()
  }

  @Provides @Singleton fun provideService(retrofit: Retrofit) : GitService {
    return retrofit.create(GitService::class.java)
  }
}
