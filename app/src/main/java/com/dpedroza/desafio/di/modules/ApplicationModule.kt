package com.dpedroza.desafio.di.modules

import android.content.Context
import android.content.SharedPreferences
import com.dpedroza.desafio.application.DesafioApplication
import com.dpedroza.desafio.di.scopes.ApplicationContext
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Dagger module that provides objects which will live during the application lifecycle.
 */
@Module
class ApplicationModule constructor(private val androidApplication: DesafioApplication) {

    @Provides
    @Singleton
    internal fun provideApplication(): DesafioApplication {
        return androidApplication
    }

    @Provides
    @Singleton
    @ApplicationContext
    internal fun provideApplicationContext(): Context {
        return androidApplication
    }

    @Provides
    @Singleton
    internal fun provideSharedPreferences(): SharedPreferences {
        return androidApplication.getSharedPreferences("app", Context.MODE_APPEND)
    }

}
