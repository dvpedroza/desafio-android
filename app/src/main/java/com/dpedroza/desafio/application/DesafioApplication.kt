package com.dpedroza.desafio.application

import android.app.Application
import com.dpedroza.desafio.di.components.ApplicationComponent
import com.dpedroza.desafio.di.components.DaggerApplicationComponent
import com.dpedroza.desafio.di.modules.ApplicationModule

class DesafioApplication : Application() {

  companion object {

    @JvmStatic lateinit var graph: ApplicationComponent

  }

  override fun onCreate() {
    super.onCreate()
    initDagger()
  }

  private fun initDagger() {
    graph = DaggerApplicationComponent.builder()
        .applicationModule(ApplicationModule(this))
        .build()
  }


}