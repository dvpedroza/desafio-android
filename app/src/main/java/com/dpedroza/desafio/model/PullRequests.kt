package com.dpedroza.desafio.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class PullRequests(val title: String, val body: String, val user: User, val updatedAt: String, val state: String)  : Parcelable {

  constructor(source: Parcel): this(source.readString(), source.readString(), source.readParcelable<User>(User::class.java.classLoader), source.readString(), source.readString())

  override fun writeToParcel(dest: Parcel?, flags: Int) {
    dest?.writeString(this.title)
    dest?.writeString(this.body)
    dest?.writeParcelable(this.user, 0)
    dest?.writeString(this.updatedAt)
    dest?.writeString(this.state)
  }

  override fun describeContents(): Int {
    return 0
  }

  companion object {
    @JvmField val CREATOR: Parcelable.Creator<Owner> = object : Parcelable.Creator<Owner> {
      override fun createFromParcel(source: Parcel): Owner{
        return Owner(source)
      }

      override fun newArray(size: Int): Array<Owner?> {
        return arrayOfNulls(size)
      }
    }
  }

  @SerializedName("url") var url: String? = null
  @SerializedName("id") var id: Int = 0
  @SerializedName("html_url") var htmlUrl: String? = null
  @SerializedName("diff_url") var diffUrl: String? = null
  @SerializedName("patch_url") var patchUrl: String? = null
  @SerializedName("issue_url") var issueUrl: String? = null
  @SerializedName("number") var number: Int = 0
  @SerializedName("locked") var isLocked: Boolean = false
  @SerializedName("created_at") var createdAt: String? = null
  @SerializedName("closed_at") var closedAt: Any? = null
  @SerializedName("merged_at") var mergedAt: Any? = null
  @SerializedName("merge_commit_sha") var mergeCommitSha: String? = null
  @SerializedName("assignee") var assignee: Any? = null
  @SerializedName("milestone") var milestone: Any? = null
  @SerializedName("commits_url") var commitsUrl: String? = null
  @SerializedName("review_comments_url") var reviewCommentsUrl: String? = null
  @SerializedName("review_comment_url") var reviewCommentUrl: String? = null
  @SerializedName("comments_url") var commentsUrl: String? = null
  @SerializedName("statuses_url") var statusesUrl: String? = null
  @SerializedName("head") var head: Head? = null
  @SerializedName("base") var base: Base? = null
  @SerializedName("_links") var links: Links? = null
  @SerializedName("assignees") var assignees: List<*>? = null
  @SerializedName("requested_reviewers") var requestedReviewers: List<*>? = null

  class User(val id: Int, val login: String, @SerializedName("avatar_url") val avatarUrl: String?) : Parcelable {

    constructor(source: Parcel): this(source.readInt(), source.readString(), source.readString())

    override fun writeToParcel(dest: Parcel?, flags: Int) {
      dest?.writeInt(this.id)
      dest?.writeString(this.login)
      dest?.writeString(this.avatarUrl)
    }

    override fun describeContents(): Int {
      return 0
    }

    companion object {
      @JvmField val CREATOR: Parcelable.Creator<Owner> = object : Parcelable.Creator<Owner> {
        override fun createFromParcel(source: Parcel): Owner{
          return Owner(source)
        }

        override fun newArray(size: Int): Array<Owner?> {
          return arrayOfNulls(size)
        }
      }
    }

    @SerializedName("gravatar_id") var gravatarId: String? = null
    @SerializedName("url") var url: String? = null
    @SerializedName("html_url") var htmlUrl: String? = null
    @SerializedName("followers_url") var followersUrl: String? = null
    @SerializedName("following_url") var followingUrl: String? = null
    @SerializedName("gists_url") var gistsUrl: String? = null
    @SerializedName("starred_url") var starredUrl: String? = null
    @SerializedName("subscriptions_url") var subscriptionsUrl: String? = null
    @SerializedName("organizations_url") var organizationsUrl: String? = null
    @SerializedName("repos_url") var reposUrl: String? = null
    @SerializedName("events_url") var eventsUrl: String? = null
    @SerializedName("received_events_url") var receivedEventsUrl: String? = null
    @SerializedName("type") var type: String? = null
    @SerializedName("site_admin") var isSiteAdmin: Boolean = false
  }

  class Head {

    @SerializedName("label") var label: String? = null
    @SerializedName("ref") var ref: String? = null
    @SerializedName("sha") var sha: String? = null
    @SerializedName("user") var user: UserX? = null
    @SerializedName("repo") var repo: Repo? = null

    class UserX {

      @SerializedName("login") var login: String? = null
      @SerializedName("id") var id: Int = 0
      @SerializedName("avatar_url") var avatarUrl: String? = null
      @SerializedName("gravatar_id") var gravatarId: String? = null
      @SerializedName("url") var url: String? = null
      @SerializedName("html_url") var htmlUrl: String? = null
      @SerializedName("followers_url") var followersUrl: String? = null
      @SerializedName("following_url") var followingUrl: String? = null
      @SerializedName("gists_url") var gistsUrl: String? = null
      @SerializedName("starred_url") var starredUrl: String? = null
      @SerializedName("subscriptions_url") var subscriptionsUrl: String? = null
      @SerializedName("organizations_url") var organizationsUrl: String? = null
      @SerializedName("repos_url") var reposUrl: String? = null
      @SerializedName("events_url") var eventsUrl: String? = null
      @SerializedName("received_events_url") var receivedEventsUrl: String? = null
      @SerializedName("type") var type: String? = null
      @SerializedName("site_admin") var isSiteAdmin: Boolean = false
    }

    class Repo {

      @SerializedName("id") var id: Int = 0
      @SerializedName("name") var name: String? = null
      @SerializedName("full_name") var fullName: String? = null
      @SerializedName("owner") var owner: Owner? = null
      @SerializedName("private") var isPrivateX: Boolean = false
      @SerializedName("html_url") var htmlUrl: String? = null
      @SerializedName("description") var description: String? = null
      @SerializedName("fork") var isFork: Boolean = false
      @SerializedName("url") var url: String? = null
      @SerializedName("forks_url") var forksUrl: String? = null
      @SerializedName("keys_url") var keysUrl: String? = null
      @SerializedName("collaborators_url") var collaboratorsUrl: String? = null
      @SerializedName("teams_url") var teamsUrl: String? = null
      @SerializedName("hooks_url") var hooksUrl: String? = null
      @SerializedName("issue_events_url") var issueEventsUrl: String? = null
      @SerializedName("events_url") var eventsUrl: String? = null
      @SerializedName("assignees_url") var assigneesUrl: String? = null
      @SerializedName("branches_url") var branchesUrl: String? = null
      @SerializedName("tags_url") var tagsUrl: String? = null
      @SerializedName("blobs_url") var blobsUrl: String? = null
      @SerializedName("git_tags_url") var gitTagsUrl: String? = null
      @SerializedName("git_refs_url") var gitRefsUrl: String? = null
      @SerializedName("trees_url") var treesUrl: String? = null
      @SerializedName("statuses_url") var statusesUrl: String? = null
      @SerializedName("languages_url") var languagesUrl: String? = null
      @SerializedName("stargazers_url") var stargazersUrl: String? = null
      @SerializedName("contributors_url") var contributorsUrl: String? = null
      @SerializedName("subscribers_url") var subscribersUrl: String? = null
      @SerializedName("subscription_url") var subscriptionUrl: String? = null
      @SerializedName("commits_url") var commitsUrl: String? = null
      @SerializedName("git_commits_url") var gitCommitsUrl: String? = null
      @SerializedName("comments_url") var commentsUrl: String? = null
      @SerializedName("issue_comment_url") var issueCommentUrl: String? = null
      @SerializedName("contents_url") var contentsUrl: String? = null
      @SerializedName("compare_url") var compareUrl: String? = null
      @SerializedName("merges_url") var mergesUrl: String? = null
      @SerializedName("archive_url") var archiveUrl: String? = null
      @SerializedName("downloads_url") var downloadsUrl: String? = null
      @SerializedName("issues_url") var issuesUrl: String? = null
      @SerializedName("pulls_url") var pullsUrl: String? = null
      @SerializedName("milestones_url") var milestonesUrl: String? = null
      @SerializedName("notifications_url") var notificationsUrl: String? = null
      @SerializedName("labels_url") var labelsUrl: String? = null
      @SerializedName("releases_url") var releasesUrl: String? = null
      @SerializedName("deployments_url") var deploymentsUrl: String? = null
      @SerializedName("created_at") var createdAt: String? = null
      @SerializedName("updated_at") var updatedAt: String? = null
      @SerializedName("pushed_at") var pushedAt: String? = null
      @SerializedName("git_url") var gitUrl: String? = null
      @SerializedName("ssh_url") var sshUrl: String? = null
      @SerializedName("clone_url") var cloneUrl: String? = null
      @SerializedName("svn_url") var svnUrl: String? = null
      @SerializedName("homepage") var homepage: String? = null
      @SerializedName("size") var size: Int = 0
      @SerializedName("stargazers_count") var stargazersCount: Int = 0
      @SerializedName("watchers_count") var watchersCount: Int = 0
      @SerializedName("language") var language: String? = null
      @SerializedName("has_issues") var isHasIssues: Boolean = false
      @SerializedName("has_projects") var isHasProjects: Boolean = false
      @SerializedName("has_downloads") var isHasDownloads: Boolean = false
      @SerializedName("has_wiki") var isHasWiki: Boolean = false
      @SerializedName("has_pages") var isHasPages: Boolean = false
      @SerializedName("forks_count") var forksCount: Int = 0
      @SerializedName("mirror_url") var mirrorUrl: Any? = null
      @SerializedName("open_issues_count") var openIssuesCount: Int = 0
      @SerializedName("forks") var forks: Int = 0
      @SerializedName("open_issues") var openIssues: Int = 0
      @SerializedName("watchers") var watchers: Int = 0
      @SerializedName("default_branch") var defaultBranch: String? = null

      class Owner {

        @SerializedName("login") var login: String? = null
        @SerializedName("id") var id: Int = 0
        @SerializedName("avatar_url") var avatarUrl: String? = null
        @SerializedName("gravatar_id") var gravatarId: String? = null
        @SerializedName("url") var url: String? = null
        @SerializedName("html_url") var htmlUrl: String? = null
        @SerializedName("followers_url") var followersUrl: String? = null
        @SerializedName("following_url") var followingUrl: String? = null
        @SerializedName("gists_url") var gistsUrl: String? = null
        @SerializedName("starred_url") var starredUrl: String? = null
        @SerializedName("subscriptions_url") var subscriptionsUrl: String? = null
        @SerializedName("organizations_url") var organizationsUrl: String? = null
        @SerializedName("repos_url") var reposUrl: String? = null
        @SerializedName("events_url") var eventsUrl: String? = null
        @SerializedName("received_events_url") var receivedEventsUrl: String? = null
        @SerializedName("type") var type: String? = null
        @SerializedName("site_admin") var isSiteAdmin: Boolean = false
      }
    }
  }

  class Base {

    @SerializedName("label") var label: String? = null
    @SerializedName("ref") var ref: String? = null
    @SerializedName("sha") var sha: String? = null
    @SerializedName("user") var user: UserXX? = null
    @SerializedName("repo") var repo: RepoX? = null

    class UserXX {

      @SerializedName("login") var login: String? = null
      @SerializedName("id") var id: Int = 0
      @SerializedName("avatar_url") var avatarUrl: String? = null
      @SerializedName("gravatar_id") var gravatarId: String? = null
      @SerializedName("url") var url: String? = null
      @SerializedName("html_url") var htmlUrl: String? = null
      @SerializedName("followers_url") var followersUrl: String? = null
      @SerializedName("following_url") var followingUrl: String? = null
      @SerializedName("gists_url") var gistsUrl: String? = null
      @SerializedName("starred_url") var starredUrl: String? = null
      @SerializedName("subscriptions_url") var subscriptionsUrl: String? = null
      @SerializedName("organizations_url") var organizationsUrl: String? = null
      @SerializedName("repos_url") var reposUrl: String? = null
      @SerializedName("events_url") var eventsUrl: String? = null
      @SerializedName("received_events_url") var receivedEventsUrl: String? = null
      @SerializedName("type") var type: String? = null
      @SerializedName("site_admin") var isSiteAdmin: Boolean = false
    }

    class RepoX {

      @SerializedName("id") var id: Int = 0
      @SerializedName("name") var name: String? = null
      @SerializedName("full_name") var fullName: String? = null
      @SerializedName("owner") var owner: OwnerX? = null
      @SerializedName("private") var isPrivateX: Boolean = false
      @SerializedName("html_url") var htmlUrl: String? = null
      @SerializedName("description") var description: String? = null
      @SerializedName("fork") var isFork: Boolean = false
      @SerializedName("url") var url: String? = null
      @SerializedName("forks_url") var forksUrl: String? = null
      @SerializedName("keys_url") var keysUrl: String? = null
      @SerializedName("collaborators_url") var collaboratorsUrl: String? = null
      @SerializedName("teams_url") var teamsUrl: String? = null
      @SerializedName("hooks_url") var hooksUrl: String? = null
      @SerializedName("issue_events_url") var issueEventsUrl: String? = null
      @SerializedName("events_url") var eventsUrl: String? = null
      @SerializedName("assignees_url") var assigneesUrl: String? = null
      @SerializedName("branches_url") var branchesUrl: String? = null
      @SerializedName("tags_url") var tagsUrl: String? = null
      @SerializedName("blobs_url") var blobsUrl: String? = null
      @SerializedName("git_tags_url") var gitTagsUrl: String? = null
      @SerializedName("git_refs_url") var gitRefsUrl: String? = null
      @SerializedName("trees_url") var treesUrl: String? = null
      @SerializedName("statuses_url") var statusesUrl: String? = null
      @SerializedName("languages_url") var languagesUrl: String? = null
      @SerializedName("stargazers_url") var stargazersUrl: String? = null
      @SerializedName("contributors_url") var contributorsUrl: String? = null
      @SerializedName("subscribers_url") var subscribersUrl: String? = null
      @SerializedName("subscription_url") var subscriptionUrl: String? = null
      @SerializedName("commits_url") var commitsUrl: String? = null
      @SerializedName("git_commits_url") var gitCommitsUrl: String? = null
      @SerializedName("comments_url") var commentsUrl: String? = null
      @SerializedName("issue_comment_url") var issueCommentUrl: String? = null
      @SerializedName("contents_url") var contentsUrl: String? = null
      @SerializedName("compare_url") var compareUrl: String? = null
      @SerializedName("merges_url") var mergesUrl: String? = null
      @SerializedName("archive_url") var archiveUrl: String? = null
      @SerializedName("downloads_url") var downloadsUrl: String? = null
      @SerializedName("issues_url") var issuesUrl: String? = null
      @SerializedName("pulls_url") var pullsUrl: String? = null
      @SerializedName("milestones_url") var milestonesUrl: String? = null
      @SerializedName("notifications_url") var notificationsUrl: String? = null
      @SerializedName("labels_url") var labelsUrl: String? = null
      @SerializedName("releases_url") var releasesUrl: String? = null
      @SerializedName("deployments_url") var deploymentsUrl: String? = null
      @SerializedName("created_at") var createdAt: String? = null
      @SerializedName("updated_at") var updatedAt: String? = null
      @SerializedName("pushed_at") var pushedAt: String? = null
      @SerializedName("git_url") var gitUrl: String? = null
      @SerializedName("ssh_url") var sshUrl: String? = null
      @SerializedName("clone_url") var cloneUrl: String? = null
      @SerializedName("svn_url") var svnUrl: String? = null
      @SerializedName("homepage") var homepage: String? = null
      @SerializedName("size") var size: Int = 0
      @SerializedName("stargazers_count") var stargazersCount: Int = 0
      @SerializedName("watchers_count") var watchersCount: Int = 0
      @SerializedName("language") var language: String? = null
      @SerializedName("has_issues") var isHasIssues: Boolean = false
      @SerializedName("has_projects") var isHasProjects: Boolean = false
      @SerializedName("has_downloads") var isHasDownloads: Boolean = false
      @SerializedName("has_wiki") var isHasWiki: Boolean = false
      @SerializedName("has_pages") var isHasPages: Boolean = false
      @SerializedName("forks_count") var forksCount: Int = 0
      @SerializedName("mirror_url") var mirrorUrl: Any? = null
      @SerializedName("open_issues_count") var openIssuesCount: Int = 0
      @SerializedName("forks") var forks: Int = 0
      @SerializedName("open_issues") var openIssues: Int = 0
      @SerializedName("watchers") var watchers: Int = 0
      @SerializedName("default_branch") var defaultBranch: String? = null

      class OwnerX {

        @SerializedName("login") var login: String? = null
        @SerializedName("id") var id: Int = 0
        @SerializedName("avatar_url") var avatarUrl: String? = null
        @SerializedName("gravatar_id") var gravatarId: String? = null
        @SerializedName("url") var url: String? = null
        @SerializedName("html_url") var htmlUrl: String? = null
        @SerializedName("followers_url") var followersUrl: String? = null
        @SerializedName("following_url") var followingUrl: String? = null
        @SerializedName("gists_url") var gistsUrl: String? = null
        @SerializedName("starred_url") var starredUrl: String? = null
        @SerializedName("subscriptions_url") var subscriptionsUrl: String? = null
        @SerializedName("organizations_url") var organizationsUrl: String? = null
        @SerializedName("repos_url") var reposUrl: String? = null
        @SerializedName("events_url") var eventsUrl: String? = null
        @SerializedName("received_events_url") var receivedEventsUrl: String? = null
        @SerializedName("type") var type: String? = null
        @SerializedName("site_admin") var isSiteAdmin: Boolean = false
      }
    }
  }

  class Links {

    @SerializedName("self") var self: Self? = null
    @SerializedName("html") var html: Html? = null
    @SerializedName("issue") var issue: Issue? = null
    @SerializedName("comments") var comments: Comments? = null
    @SerializedName("review_comments") var reviewComments: ReviewComments? = null
    @SerializedName("review_comment") var reviewComment: ReviewComment? = null
    @SerializedName("commits") var commits: Commits? = null
    @SerializedName("statuses") var statuses: Statuses? = null



    class Issue {

      @SerializedName("href") var href: String? = null
    }

    class Comments {

      @SerializedName("href") var href: String? = null
    }

    class ReviewComments {

      @SerializedName("href") var href: String? = null
    }

    class ReviewComment {

      @SerializedName("href") var href: String? = null
    }

    class Commits {

      @SerializedName("href") var href: String? = null
    }

    class Statuses {

      @SerializedName("href") var href: String? = null
    }
  }
}
