package com.dpedroza.desafio.model

import com.google.gson.annotations.SerializedName

class Self {
  /**
   * href : https://api.github.com/repos/elastic/elasticsearch/pulls/24817
   */

  @SerializedName("href") var href: String? = null
}