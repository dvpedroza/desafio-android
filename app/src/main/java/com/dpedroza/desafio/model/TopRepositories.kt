package com.dpedroza.desafio.model

import com.google.gson.annotations.SerializedName

class TopRepositories {

  @SerializedName("total_count") var totalCount: Int = 0
  @SerializedName("incomplete_results") var incompleteResults: Boolean = false
  @SerializedName("items") var items: List<Item> = ArrayList()

}