package com.dpedroza.desafio.model

import android.os.Parcel
import android.os.Parcelable
import com.google.gson.annotations.SerializedName

class Owner (var id: Int): Parcelable {

  constructor(source: Parcel): this(source.readInt())

  override fun writeToParcel(dest: Parcel?, flags: Int) {
    dest?.writeInt(this.id)
  }

  override fun describeContents(): Int {
    return 0
  }

  companion object {
    @JvmField val CREATOR: Parcelable.Creator<Owner> = object : Parcelable.Creator<Owner> {
      override fun createFromParcel(source: Parcel): Owner{
        return Owner(source)
      }

      override fun newArray(size: Int): Array<Owner?> {
        return arrayOfNulls(size)
      }
    }
  }

  @SerializedName("login") var login: String? = null
  @SerializedName("avatar_url") var avatarUrl: String? = null
  @SerializedName("gravatar_id") var gravatarId: String? = null
  @SerializedName("url") var url: String? = null
  @SerializedName("html_url") var htmlUrl: String? = null
  @SerializedName("followers_url") var followersUrl: String? = null
  @SerializedName("following_url") var followingUrl: String? = null
  @SerializedName("gists_url") var gistsUrl: String? = null
  @SerializedName("starred_url") var starredUrl: String? = null
  @SerializedName("subscriptions_url") var subscriptionsUrl: String? = null
  @SerializedName("organizations_url") var organizationsUrl: String? = null
  @SerializedName("repos_url") var reposUrl: String? = null
  @SerializedName("events_url") var eventsUrl: String? = null
  @SerializedName("received_events_url") var receivedEventsUrl: String? = null
  @SerializedName("type") var type: String? = null
  @SerializedName("site_admin") var isSiteAdmin: Boolean = false
}
