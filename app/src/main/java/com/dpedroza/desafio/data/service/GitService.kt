package com.dpedroza.desafio.data.service

import com.dpedroza.desafio.model.PullRequests
import com.dpedroza.desafio.model.TopRepositories
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface GitService {

  @GET("search/repositories?")
  fun getTopRepositories(@Query("q") language: String, @Query("sort") sort: String, @Query("page") pageId: Int) : Observable<TopRepositories>


  @GET("repos/{owner}/{repositoryName}/pulls")
  fun getPullRequests(@Path("owner") owner: String, @Path("repositoryName") repository: String) : Observable<List<PullRequests>>

}