package com.dpedroza.desafio.data.repository

import com.dpedroza.desafio.model.TopRepositories
import io.reactivex.Observable

interface GitRepository {

  fun getTopRespositories(pageId: Int) : Observable<TopRepositories>

}