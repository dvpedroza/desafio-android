package com.dpedroza.desafio.data.repository.manager

import com.dpedroza.desafio.data.repository.PullRequestRepository
import com.dpedroza.desafio.data.service.GitService
import com.dpedroza.desafio.model.PullRequests
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class PullRequestManager @Inject constructor(val service: GitService): PullRequestRepository {

  override fun getPullRequests(owner: String, repositoryName: String) : Observable<List<PullRequests>> {
    return service.getPullRequests(owner, repositoryName)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
  }

}