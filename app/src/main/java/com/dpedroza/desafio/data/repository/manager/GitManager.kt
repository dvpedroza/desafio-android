package com.dpedroza.desafio.data.repository.manager

import com.dpedroza.desafio.data.repository.GitRepository
import com.dpedroza.desafio.data.service.GitService
import com.dpedroza.desafio.model.TopRepositories
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject

class GitManager @Inject constructor(val service: GitService): GitRepository {

  override fun getTopRespositories(pageId: Int): Observable<TopRepositories> {
    return service.getTopRepositories("language:Java", "stars", pageId)
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
  }

}