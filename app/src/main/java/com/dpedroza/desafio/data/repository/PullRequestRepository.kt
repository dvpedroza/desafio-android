package com.dpedroza.desafio.data.repository

import com.dpedroza.desafio.model.PullRequests
import io.reactivex.Observable

interface PullRequestRepository {

  fun getPullRequests(owner: String, repositoryName: String) : Observable<List<PullRequests>>

}