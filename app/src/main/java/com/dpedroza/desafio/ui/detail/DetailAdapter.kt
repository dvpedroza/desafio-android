package com.dpedroza.desafio.ui.detail

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dpedroza.desafio.R
import com.dpedroza.desafio.model.PullRequests
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_pull_request.view.*

class DetailAdapter(val context: Context) : RecyclerView.Adapter<DetailAdapter.ViewHolder>() {

  val pullRequests: MutableList<PullRequests> = mutableListOf()

  fun setPullRequestList(list: List<PullRequests>) {
    pullRequests.addAll(list)
    notifyItemRangeChanged(itemCount, pullRequests.size)
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
    val view = LayoutInflater.from(parent.context).inflate(R.layout.item_pull_request, parent, false)
    return ViewHolder(view)
  }

  override fun onBindViewHolder(holder: ViewHolder, position: Int) {
    holder.bind(pullRequests[position])
  }

  override fun getItemCount() = pullRequests.size

  inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind(pullRequest: PullRequests) {
      with(pullRequest) {

        itemView.tvItemPRTitle.text = pullRequest.title
        itemView.tvItemPRBody.text = pullRequest.body
        itemView.tvItemPRAuthorNick.text = pullRequest.user.login
        itemView.tvItemPRUserType.text = pullRequest.user.type
        itemView.tvItemPRDate.text = pullRequest.updatedAt

        Picasso.with(context)
            .load(pullRequest.user.avatarUrl)
            .placeholder(R.drawable.ic_account_circle_gray_24dp)
            .into(itemView.imgItemPRAuthorPicture)

      }
    }
  }
}