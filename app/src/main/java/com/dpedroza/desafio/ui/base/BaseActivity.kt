package com.dpedroza.desafio.ui.base

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.dpedroza.desafio.application.DesafioApplication
import com.dpedroza.desafio.di.components.ActivityComponent


open class BaseActivity : AppCompatActivity() {

  protected var activityComponent: ActivityComponent = DesafioApplication.graph.activityComponent()

    @Suppress("UsePropertyAccessSyntax")
    override fun onCreate(savedInstanceState: Bundle?) {
      super.onCreate(savedInstanceState)
    }

}
