package com.dpedroza.desafio.ui.main

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.dpedroza.desafio.R
import com.dpedroza.desafio.model.Item
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_load_progress.view.*
import kotlinx.android.synthetic.main.item_top_repository.view.*

class MainAdapter(val context: Context, val itemClick: (Item) -> Unit) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

  val repositoriesList: MutableList<Item> = mutableListOf()
  private val VIEW_TYPE_ITEM = 0
  private val VIEW_TYPE_LOAD = 1

  override fun onBindViewHolder(holder: RecyclerView.ViewHolder?, position: Int) {

    when (position < itemCount - 1) {
      true -> {
        val viewHolder = holder as ViewHolder
        viewHolder.bind(repositoriesList[position])
      }
      false -> {
        val viewHolder = holder as ProgressViewHolder
        viewHolder.bind()
      }
    }

  }

  fun setRepositoryList(list: List<Item>) {
    repositoriesList.addAll(list)
    notifyItemRangeChanged(itemCount, repositoriesList.size)
  }

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {

    when (viewType) {
      VIEW_TYPE_ITEM -> return ViewHolder(
          LayoutInflater.from(parent.context).inflate(R.layout.item_top_repository, parent, false),
          itemClick)
      VIEW_TYPE_LOAD -> return ProgressViewHolder(
          LayoutInflater.from(parent.context).inflate(R.layout.item_load_progress, parent, false))
    }

    return ProgressViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_load_progress, parent, false))

  }

  override fun getItemViewType(position: Int): Int {
    when (position < itemCount - 1) {
      true -> return VIEW_TYPE_ITEM
      false -> return VIEW_TYPE_LOAD
    }
  }

  override fun getItemCount() = repositoriesList.size

  inner class ViewHolder(view: View, val itemClick: (Item) -> Unit) : RecyclerView.ViewHolder(
      view) {

    fun bind(item: Item) {
      with(item) {

        itemView.tvItemRepoName.text = item.name
        itemView.tvItemRepoDescription.text = item.description
        itemView.tvItemRepoForkNumber.text = item.forksCount.toString()
        itemView.tvItemRepoAuthorNick.text = item.owner.login
        itemView.tvItemRepoNameFull.text = item.fullName
        itemView.setOnClickListener { itemClick(this) }

        Picasso.with(context)
            .load(item.owner.avatarUrl)
            .placeholder(R.drawable.ic_account_circle_gray_24dp)
            .into(itemView.imgItemRepoAuthorPicture)

      }
    }
  }

  inner class ProgressViewHolder(view: View) : RecyclerView.ViewHolder(view) {

    fun bind() {
      itemView.progress.animate()
    }

  }
}
