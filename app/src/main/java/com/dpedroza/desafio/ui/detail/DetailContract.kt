package com.dpedroza.desafio.ui.detail

import com.dpedroza.desafio.model.PullRequests
import com.dpedroza.desafio.ui.base.BaseMvpPresenter
import com.dpedroza.desafio.ui.base.MvpView

object DetailContract {

    interface View : MvpView {

      fun retry()
      fun onLoadStarted()
      fun onLoadFinished()
      fun showPullRequests(pullRequests: List<PullRequests>)


    }

    abstract class Presenter : BaseMvpPresenter<View>() {

      abstract fun getPullRequests(owner: String, repositoryName: String)

    }

}