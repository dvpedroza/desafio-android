package com.dpedroza.desafio.ui.detail

import com.dpedroza.desafio.data.repository.PullRequestRepository
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

class DetailPresenter @Inject constructor(val repository: PullRequestRepository) : DetailContract.Presenter() {

  override fun getPullRequests(owner: String, repositoryName: String) {

    view.onLoadStarted()

    addDisposable(repository.getPullRequests(owner, repositoryName)
        .subscribeBy(
            onNext = {view.showPullRequests(it)},
            onError = {view.onShowError("Failed to get pull requests") ; view.onLoadFinished()},
            onComplete = {view.onLoadFinished()}
        ))
  }

}