package com.dpedroza.desafio.ui.main

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.os.Parcelable
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.dpedroza.desafio.R
import com.dpedroza.desafio.model.Item
import com.dpedroza.desafio.model.TopRepositories
import com.dpedroza.desafio.ui.base.BaseActivity
import com.dpedroza.desafio.ui.components.EndlessRecyclerViewScrollListener
import com.dpedroza.desafio.ui.detail.DetailActivity
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.toolbar.*
import javax.inject.Inject

class MainActivity : BaseActivity(), MainContract.View {

  @Inject lateinit var presenter: MainPresenter

  private var pageId: Int = 1
  private lateinit var loadDialog : ProgressDialog
  private val adapter = MainAdapter(this, { onRepositoryClicked(it) })
  private var endlessRecyclerViewScrollListener: EndlessRecyclerViewScrollListener? = null

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_main)
    setupDagger()
    setupToolbar()
    setupProgressDialog()
    setupPresenter()
    setupRecyclerView()
  }

  override fun onResume() {
    super.onResume()
    if (adapter.itemCount == 0) {
        presenter.getTopRepositories(pageId, true)
    }
  }

  override fun onSaveInstanceState(outState: Bundle?) {
    super.onSaveInstanceState(outState)
    outState?.putParcelableArrayList("repositories_list", adapter.repositoriesList as ArrayList<out Parcelable>)
    outState?.putInt("pageId", pageId)
  }

  override fun onRestoreInstanceState(savedInstanceState: Bundle?) {
    super.onRestoreInstanceState(savedInstanceState)
    val page = savedInstanceState?.getInt("pageId")
    pageId = page as Int
    adapter.setRepositoryList(savedInstanceState.getParcelableArrayList<Item>("repositories_list") as List<Item>)
  }

  private fun setupDagger() {
    activityComponent.inject(this)
  }

  private fun setupProgressDialog() {
    loadDialog = ProgressDialog(this)
    loadDialog.setMessage("Carregando...")
    loadDialog.setCancelable(false)
  }

  private fun setupRecyclerView() {
    val linearLayoutManager = LinearLayoutManager(this)
    rvRepositories.layoutManager = linearLayoutManager
    rvRepositories.adapter = adapter
    endlessRecyclerViewScrollListener = getEndlessRecyclerViewScrollListener(linearLayoutManager)
    rvRepositories.addOnScrollListener(endlessRecyclerViewScrollListener)
  }

  private fun getEndlessRecyclerViewScrollListener(
      linearLayoutManager: LinearLayoutManager): EndlessRecyclerViewScrollListener {
    return object : EndlessRecyclerViewScrollListener(linearLayoutManager) {
      override fun onLoadMore(page: Int, totalItemsCount: Int, view: RecyclerView?) {
        presenter.getTopRepositories(++pageId, false)
      }
    }
  }

  private fun setupPresenter() {
    presenter.attachView(this)
  }

  private fun setupToolbar() {
    toolbar.title = "Github JavaPop"
    toolbar.setTitleTextColor(Color.WHITE)
  }

  override fun showTopRepositories(topRepositories: TopRepositories) {
    adapter.setRepositoryList(topRepositories.items)
  }

  override fun retry() {
    adapter.repositoriesList.clear()
    presenter.getTopRepositories(pageId, true)
  }

  override fun onLoadStarted() {
    loadDialog.show()
  }

  override fun onLoadFinished() {
    loadDialog.dismiss()
  }

  override fun onShowError(errorMessage: String) {
    Snackbar.make(findViewById(android.R.id.content), errorMessage, Snackbar.LENGTH_INDEFINITE)
        .setAction("Tentar Novamente", { retry() })
        .show()
  }

  private fun onRepositoryClicked(item: Item): Unit {
    val intent = Intent(this, DetailActivity::class.java)
    intent.putExtra("full_name", item.fullName)
    intent.putExtra("owner_login", item.owner.login)
    intent.putExtra("repository_name", item.name)
    startActivity(intent)
  }

}
