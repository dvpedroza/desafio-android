package com.dpedroza.desafio.ui.main

import com.dpedroza.desafio.model.TopRepositories
import com.dpedroza.desafio.ui.base.BaseMvpPresenter
import com.dpedroza.desafio.ui.base.MvpView

object MainContract {

    interface View : MvpView {

      fun retry()
      fun onLoadStarted()
      fun onLoadFinished()
      fun showTopRepositories(topRepositories: TopRepositories)

    }

    abstract class Presenter : BaseMvpPresenter<View>() {

      abstract fun getTopRepositories(pageId: Int, showDialog: Boolean)

    }

}