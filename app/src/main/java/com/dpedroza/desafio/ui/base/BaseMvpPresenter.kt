package com.dpedroza.desafio.ui.base

import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable

/**
 * Base class that implements the Presenter interface and provides a base implementation for
 * attachView() and detachView(). It also handles keeping a reference to the [_view] that
 * can be accessed from the children classes by calling [view].
 */
open class BaseMvpPresenter<T : MvpView> : MvpPresenter<T> {

  protected var disposables: CompositeDisposable? = null

  private var _view: T? = null
  val view: T
    get() {
      return _view ?: throw MvpViewNotAttachedException()
    }

  override fun attachView(view: T) {
    _view = view
    disposables = CompositeDisposable()
  }

  override fun detachView() {
    _view = null
    disposables?.clear()
  }

  protected fun addDisposable(disposable: Disposable) {
    disposables?.add(disposable)
  }


  class MvpViewNotAttachedException : RuntimeException(
      "Please call Presenter.attachView(MvpView) before requesting data to the Presenter")
}
