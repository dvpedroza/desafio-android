package com.dpedroza.desafio.ui.main

import com.dpedroza.desafio.data.repository.GitRepository
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

open class MainPresenter
@Inject
constructor(var gitRepository: GitRepository) : MainContract.Presenter() {

  override fun getTopRepositories(pageId: Int, showDialog: Boolean) {

      if (showDialog) view.onLoadStarted()

      addDisposable(gitRepository.getTopRespositories(pageId)
          .subscribeBy(
              onNext = { view.showTopRepositories(it) },
              onError = {view.onShowError("Failed to get repositories") ; view.onLoadFinished()},
              onComplete = { view.onLoadFinished()}
          ))
  }

}