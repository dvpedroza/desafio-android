package com.dpedroza.desafio.ui.detail

import android.app.ProgressDialog
import android.graphics.Color
import android.os.Bundle
import android.os.Parcelable
import android.support.design.widget.Snackbar
import android.support.v7.widget.LinearLayoutManager
import android.view.View.GONE
import android.view.View.VISIBLE
import com.dpedroza.desafio.R
import com.dpedroza.desafio.model.PullRequests
import com.dpedroza.desafio.ui.base.BaseActivity
import kotlinx.android.synthetic.main.activity_detail.*
import kotlinx.android.synthetic.main.toolbar.*
import javax.inject.Inject

class DetailActivity : BaseActivity(), DetailContract.View {

  @Inject lateinit var presenter: DetailPresenter

  private val PULL_REQUEST_KEY = "pull_requests_list"
  private val adapter = DetailAdapter(this)
  private lateinit var loadDialog : ProgressDialog

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_detail)
    setupDagger()
    setupToolbar()
    setupProgressDialog()
    setupPresenter()
    setupRecyclerView()
  }

  override fun onResume() {
    super.onResume()
    if (adapter.pullRequests.isEmpty()) {
      presenter.getPullRequests(intent.extras.getString("owner_login") , intent.extras.getString("repository_name"))
    }
  }

  override fun onSaveInstanceState(outState: Bundle?) {
    super.onSaveInstanceState(outState)
    outState?.putParcelableArrayList(PULL_REQUEST_KEY, adapter.pullRequests as ArrayList<out Parcelable>)
  }

  override fun onRestoreInstanceState(savedInstanceState: Bundle) {
    super.onRestoreInstanceState(savedInstanceState)
    adapter.setPullRequestList(savedInstanceState.getParcelableArrayList<PullRequests>(PULL_REQUEST_KEY) as List<PullRequests>)
  }

  private fun setupDagger() {
    activityComponent.inject(this)
  }

  private fun setupRecyclerView() {
    rvPullRequests.layoutManager = LinearLayoutManager(this)
    rvPullRequests.adapter = adapter
  }

  private fun setupPresenter() {
    presenter.attachView(this)
  }

  private fun setupProgressDialog() {
    loadDialog = ProgressDialog(this)
    loadDialog.setMessage("Carregando...")
    loadDialog.setCancelable(false)
  }

  private fun setupToolbar() {
    toolbar.setNavigationIcon(R.drawable.ic_arrow_back_white_24dp)
    toolbar.setNavigationOnClickListener { onBackPressed() }
    toolbar.title = intent.extras.getString("full_name")
    toolbar.setTitleTextColor(Color.WHITE)
  }

  override fun onShowError(errorMessage: String) {
    Snackbar.make(findViewById(android.R.id.content), errorMessage, Snackbar.LENGTH_INDEFINITE)
        .setAction("Tentar Novamente", { retry() })
        .show()
  }

  override fun retry() {
    presenter.getPullRequests(intent.extras.getString("owner_login") , intent.extras.getString("repository_name"))
  }

  override fun onLoadStarted() {
    loadDialog.show()
  }

  override fun onLoadFinished() {
    loadDialog.dismiss()
  }

  override fun showPullRequests(pullRequests: List<PullRequests>) {
    if (pullRequests.isEmpty()) {
      rvPullRequests.visibility = GONE
      tvNoPullRequests.visibility = VISIBLE
    } else {
      adapter.setPullRequestList(pullRequests)
    }
  }

}
